import streamlit as st
from dotenv import load_dotenv
from PyPDF2 import PdfReader
from langchain.chains.conversational_retrieval.base import BaseConversationalRetrievalChain
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings import OpenAIEmbeddings, HuggingFaceInstructEmbeddings
from langchain.vectorstores import FAISS
from langchain.chat_models import ChatOpenAI
from langchain.memory import ConversationBufferMemory
from langchain.chains import ConversationalRetrievalChain
from htmlTemplates import css, bot_template, user_template
from langchain.llms import HuggingFaceHub
import openai


def get_pdf_text(pdf_docs):
    """This function takes a list of PDF documents as input and returns the
    extracted text from all the pages in those documents.

    Args:
        pdf_docs (list): A list of strings representing the file paths of the PDF documents.

    Returns:
        str: A string containing the extracted text from all the pages in the PDF documents.

    Raises:
        FileNotFoundError: If any of the PDF documents specified in the input list do not exist.

    Example:
        pdf_docs = ["document1.pdf", "document2.pdf"]
        text = get_pdf_text(pdf_docs)
        print(text)

        Output:
        "This is the text extracted from document1.pdf. This is the text extracted from document2.pdf."
    """
    text = ""
    for pdf in pdf_docs:
        pdf_reader = PdfReader(pdf)
        for page in pdf_reader.pages:
            text += page.extract_text()
    return text


def get_text_chunks(text):
    """Splits a given text into chunks of specified size with a specified
    overlap.

    Args:
        text (str): The text to be split into chunks.

    Returns:
        list: A list of text chunks.

    Example:
        >>> text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, urna id aliquet lacinia, nunc nisl ultrices nunc, id lacinia nunc nisl id nisl."
        >>> get_text_chunks(text)
        ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, urna id aliquet lacinia, nunc nisl ultrices nunc,', 'id lacinia nunc nisl id nisl.']
    """
    text_splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=1000,
        chunk_overlap=200,
        length_function=len,
    )
    chunks = text_splitter.split_text(text)
    return chunks

def initialize_vectorstore(text_chunks: list = None) -> FAISS:
    """This function takes a list of text chunks as input and returns a vector
    store. The vector store is created using the OpenAIEmbeddings class, which
    generates embeddings for the text chunks. The embeddings are then used to
    create a FAISS index, which allows for efficient similarity search and
    retrieval of vectors.

    Parameters:
    - text_chunks (list): A list of text chunks for which embeddings need to be generated.

    Returns:
    - vectorstore (FAISS): A vector store object that contains the embeddings of the text chunks.

    Example:
    text_chunks = ["This is the first chunk.", "Here is the second chunk.", "And this is the third chunk."]
    vectorstore = get_vectorstore(text_chunks)
    """
    embeddings: OpenAIEmbeddings = OpenAIEmbeddings()
    if text_chunks is None:
        vectorstore = FAISS.from_texts(texts=[''], embedding=embeddings)
    else:
        vectorstore = FAISS.from_texts(texts=text_chunks, embedding=embeddings)
    # https://huggingface.co/spaces/mteb/leaderboard
    # embeddings = HuggingFaceInstructEmbeddings(model_name="thenlper/gte-large")
    return vectorstore


def get_conversation_chain(vectorstore) -> BaseConversationalRetrievalChain:
    """Creates a conversation chain using a vector store.

    Is run after a pdf is uploaded

    Args:
        vectorstore (VectorStore): The vector store used for retrieval.

    Returns:
        ConversationalRetrievalChain: The conversation chain object.
    """
    llm = ChatOpenAI()
    # llm = HuggingFaceHub(repo_id="google/flan-t5-xxl", model_kwargs={"temperature":0.5, "max_length":512})

    memory = ConversationBufferMemory(memory_key="chat_history", return_messages=True)
    conversation_chain = ConversationalRetrievalChain.from_llm(
        llm=llm,
        retriever=vectorstore.as_retriever(),
        memory=memory,
        verbose=True,  # Verbose=True prints out the context provided to the LLM
    )
    return conversation_chain


def handle_user_input(user_question):
    """Handles user input and generates a response using the OpenAI ChatGPT
    model.

    Args:
        user_question (str): The user's question or input.

    Returns:
        None: The function does not return any value. It updates the session state variables `chat_history` and `conversation`.

    Session State Variables:
        - `chat_history` (list): A list of messages exchanged between the user and the bot.
        - `conversation` (dict): The conversation object containing the user's question and the chat history.

    Raises:
        None: The function does not raise any exceptions.

    Example:
        handle_user_input("What is the weather today?")
    """
    response = st.session_state.conversation({"question": user_question})
    st.session_state.chat_history = response["chat_history"]

    # Display user's and bot's messages alternately
    for i, message in enumerate(st.session_state.chat_history):
        if i % 2 == 0:
            st.write(
                user_template.replace("{{MSG}}", message.content),
                unsafe_allow_html=True,
            )
        else:
            st.write(
                bot_template.replace("{{MSG}}", message.content), unsafe_allow_html=True
            )


def main():
    """This function is the main entry point of the program. It sets up the
    page configuration, handles user input, and processes the uploaded PDF
    documents.

    The function first loads the environment variables and sets the page configuration for the chat application.
    It then checks if the conversation and chat history are already stored in the session state. If not,
    it initializes them as None.

    The function displays a header for the chat application and a text input field for the user to ask a
    question about the documents. If the user enters a question, the function calls the `handle_user_input`
    function to process the question.

    In the sidebar, the function displays a subheader for the user's documents and a file uploader for
    uploading multiple PDFs. When the user clicks on the "Process" button, the function processes the
    uploaded PDFs. This involves extracting the text from the PDFs, splitting the text into chunks,
    creating a vector store, and creating a conversation chain.

    The function uses the `get_pdf_text` function to extract the text from the uploaded PDFs.
    It then uses the `get_text_chunks` function to split the text into chunks. The `get_vectorstore` function
    is called to create a vector store from the text chunks. Finally, the `get_conversation_chain`
    function is called to create a conversation chain using the vector store.

    The conversation chain is stored in the session state, allowing the user to continue the conversation or
    ask more questions about the documents.

    Note: The function assumes that the `load_dotenv`, `st`, `get_pdf_text`, `get_text_chunks`,
    `get_vectorstore`, and `get_conversation_chain` functions are defined elsewhere in the code.
    """
    load_dotenv()
    st.set_page_config(page_title="Chat with multiple PDFs", page_icon=":books:")
    st.write(css, unsafe_allow_html=True)
    vector_store: FAISS = initialize_vectorstore()
    st.session_state.conversation = get_conversation_chain(vector_store)

    if "conversation" not in st.session_state:
        st.session_state.conversation = None
    if "chat_history" not in st.session_state:
        st.session_state.chat_history = None

    st.header("Chat with multiple PDFs :books:")
    user_question = st.text_input("Ask a question about your documents:")
    if user_question and st.session_state:
        handle_user_input(user_question)

    with st.sidebar:
        st.subheader("Your documents")
        pdf_docs = st.file_uploader(
            "Upload your PDFs here and click on 'Process'", accept_multiple_files=True
        )
        if st.button("Process"):
            with st.spinner("Processing"):
                # get pdf text
                raw_text = get_pdf_text(pdf_docs)

                # get the text chunks
                text_chunks = get_text_chunks(raw_text)

                # Update vector store
                vector_store.add_texts(text_chunks)



if __name__ == "__main__":
    main()
