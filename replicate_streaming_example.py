import replicate

output = replicate.run(
    "replicate/llama-2-70b-chat:2c1608e18606fad2812020dc541930f2d0495ce32eee50074220b87300bc16e1",
    input={
        "prompt": "Can you write a poem about open source machine learning? "
                  "Let's make it in the style of E. E. Cummings."
    },
)
# The replicate/llama-2-70b-chat model can stream output as it's running.
# The predict method returns an iterator, and you can iterate over that output.
for item in output:
    # https://replicate.com/replicate/llama-2-70b-chat/versions/
    # 2c1608e18606fad2812020dc541930f2d0495ce32eee50074220b87300bc16e1/api#output-schema
    print(item)
